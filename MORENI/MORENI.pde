import gab.opencv.*;//cargar librerías para video, camara, detección de caras y herramienta necesaria para el open cv para cara
import java.awt.Rectangle;
import processing.video.*;

Capture camara;//variables para usar las librerías
OpenCV opencv;

PImage espejo; //variable para que la imagen se vea como espejo
color pixel; //variable para usar los pixeles
int indice; //hacerla global para poder usarla en diferentes ciclos
color colorCaraPromedio; //hacerla global para poder usarla en diferentes ciclos
int numCaptura; //variable para registro de resultados en orden de captura para guardarse como imagen

void setup(){
  fullScreen(); //pantalla para función
  //size(640,480); //pantalla para desarrollo  
  camara=new Capture(this,640,480);//cargar cámara e inicializarla
  camara.start();
  espejo=createImage(camara.width,camara.height,RGB);
  opencv=new OpenCV(this, camara.width,camara.height); //cargar openCV e inicializarlo
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
}

void draw(){
 if(camara.available()){//mostrar lo que ve la camara
  camara.read(); 
  opencv.loadImage(camara);//cargar Detección facial 
 }
  camara.loadPixels(); espejo.loadPixels(); //cargar variables PImage
  int totalPixCara=0;
    float sumaDeRojos=0;
    float sumaDeVerdes=0;
    float sumaDeAzules=0;
  
  Rectangle[] caras=opencv.detect(); //Detección facial
//--> aquí agregar si quiero mostrar el rectángulo de detección


for(int indice=0; indice<camara.width*camara.height; indice++){ //usar/"recorrer" pixeles (uno a uno para saber) 
    int columna=indice%camara.width; //recorre pixeles de X
    int fila=indice/camara.width; //recorre pixeles de Y
      int nuevaFila=fila; //Espejo
      int nuevaColumna=camara.width-1-columna; //Espejo
      int nuevoIndice=nuevaFila*camara.width+nuevaColumna; //Espejo
    
    boolean dentroDeCara=false;//declararla antes, cuando no es verdad
    for(int indiceCara=0; indiceCara<caras.length; indiceCara++){ //identifica los pixeles de la cara
     Rectangle cara=caras[indiceCara];
     int xinicio=cara.x+cara.width*15/100; //*hace la zona detectada más pequeña
     int xfinal=cara.x+cara.width*87/100;
     int yinicio=cara.y;
     int yfinal=cara.y+cara.height;
     if(columna>=xinicio && columna<=xfinal && fila>=yinicio && fila<=yfinal){
      dentroDeCara=true; //ahora es verdad, con lo anterior ya se sabe qué es dentroDeCara
     }
    }
//promedio color de pixeles de cara    
    if(dentroDeCara || caras.length<1){ //cuando dentroDeCara es verdadero OR (and) número de caras sea 0, entonces:
     espejo.pixels[nuevoIndice]=camara.pixels[indice]; //para que dentroDeCara se pinte lo que captura la camara y en espejo
      totalPixCara++; //contador de pixeles dentro de cara
      //identifica el color RGB de cada pixel para calcular después el Promedio
      sumaDeRojos += red(camara.pixels[indice]); //acumulador para promedio de R
      sumaDeVerdes += green(camara.pixels[indice]); //acumulador para promedio de G
      sumaDeAzules += blue(camara.pixels[indice]); //acumulador para promedio de B
    }
    else{
    //espejo.pixels[nuevoIndice]=colorCaraPromedio; //lo pinta absoluto; cuando no esté dentroDeCara pintar lo demás del color Promedio de la Cara
    //para alfa
     color pixelCam=camara.pixels[indice]; //nueva variable para usar pixeles de camara
     float nuevoRojo= (red(pixelCam)*0.1+red(colorCaraPromedio)*0.9); //para promedio de colores, 
     float nuevoVerde= (green(pixelCam)*0.1+green(colorCaraPromedio)*0.9); //pero con * para darle más peso a alguna de las imágenes, 
     float nuevoAzul= (blue(pixelCam)*0.1+blue(colorCaraPromedio)*0.9); //en este caso al colorCaraPromedio 
     espejo.pixels[nuevoIndice]=color(nuevoRojo,nuevoVerde,nuevoAzul); //mostrar imagen resultado del promedio cámara y colorCaraPromedio
    } 
  } 
 
  camara.updatePixels(); espejo.updatePixels(); //actualizar variables PImage
  
    int rojoPromedio; int verdePromedio; int azulPromedio; // variables para calcular:
    //calcula el color de la Cara Promedio
    rojoPromedio=int(sumaDeRojos/totalPixCara); //int: para convertir float en int
    verdePromedio=int(sumaDeVerdes/totalPixCara);
    azulPromedio=int(sumaDeAzules/totalPixCara);
    
  println("Valor RGB de Color de Cara Promedio: "+rojoPromedio+"; "+verdePromedio+"; "+azulPromedio);
  //Resultado de color Promedio "
    colorCaraPromedio=color(rojoPromedio,verdePromedio,azulPromedio);
    //convertir el color a valores HSB
    int hueCCP=int(map(hue(colorCaraPromedio),0,255,0,360));//convertir de rgb a HSB y a sus rangos con map
    int satCCP=int(map(saturation(colorCaraPromedio),0,255,0,100));
    int briCCP=int(map(brightness(colorCaraPromedio),0,255,0,100));
  println("Valor HSB del color promedio: "+hueCCP+", "+satCCP+", "+briCCP); 
 
  //image(camara,0,0,width,height); //mostrar PImage de la camara
  image(espejo, 0,0, width,height); //mostrar PImage del output en espejo

//stand by screen
  fill(59,35,11,240); //abajo de aquí banner para mail
//banner valores de colores y Moreni
  noStroke();
  rect(15, 15, width-29,height-700);
//marco - 'stroke' de pantalla
  noFill();
  strokeWeight(30);
  stroke(125,91,63);
  rect(0,0, width,height);
//texto de banner web y mail
  textSize(20);
  fill(150,45,39);
  text("eccs.world/moreni", 1050,height-25);
//texto de banner con valores
  textSize(35);
  fill(175,92,60);
  text("RGB: "+rojoPromedio+", "+verdePromedio+", "+azulPromedio, 50,55);
  text("HSB: "+hueCCP+"°, "+satCCP+"%, "+briCCP+"%", 50,100);
//texto título moreni 
  textSize(75);
  fill(178,122,87);  
  text("moreni", 992,93,-10); //pruebaX:392 y86 ; fullscreen: 892 y97
  fill(150,45,39);
  text("moreni", 995,90); //pruebaX:395 y83 ; fullscreen: 895 y94

//-->aquí agregar si quiero el color picker manual
 
    fill(colorCaraPromedio); //visualización del Color Promedio Resultado  
    strokeWeight(10); //ancho contorno
    stroke(150,45,39); // color contorno
    rect(10,height-180, width*.14,height*.21); //cuadrado con el Color Promedio Resultado

  println(caras.length); //contador de caras detectadas 
  println("total de pixeles de la cara: "+totalPixCara); //contador de total de pixeles de la cara
}


//Fijar un resultado por Participante Y REGISTRO: captura de fotos de participantes con valores
void keyPressed(){  //presionar tecla para:
 if(key=='s'){ //al presionar "s".- (S de Save and Stop)
  noLoop(); //se detiene el "void draw" y se dentendrá en un resultado
  numCaptura++;//calcula qué número de captura es, es decir, qué número de Participante es
  String hora=hour()+"."+minute()+"."+second(); //calcula la hora
  save("aResultado Persona "+numCaptura+"; hora "+hora+".png"); //salva imagen con banner
  espejo.save("bFoto Persona "+numCaptura+"; hora "+hora+".png"); //salva el resultado sin banner
  println("Persona:"+numCaptura+"; "+hora+"; guardada");
 }
 if(key=='c'){ //al presionar "c".- (C de continue)
  loop(); //empieza el "void draw" de nuevo, es decir, seguirá corriendo el programa
 }
}